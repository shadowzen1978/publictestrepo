"""
A simple module that wraps the basic python logger in a function that allows
for printing certain messages to the std.out as well.
The official documentation recommends doing this via multiple log handlers but
that seems inefficient to me.
https://docs.python.org/2/howto/logging-cookbook.html#logging-to-multiple-destinations

To change the default log file name, pass the desired log file name to
updateFileName() from the calling script.
Passing a full path is recommended.

To change the default logging level to the file, pass the desired level to
updateLogLevel().

Usage:
logHandler = mylogger.logHandler
logHandler("This is a(n) DEBUG message.", type='debug', output=0)

Author: Shadow

"""
import logging
import os
from colorama import init, Fore, Back, Style
init(autoreset=True)
cwd = os.getcwd()

GLOBAL_DEBUG = 0  # Outputs all DEBUG messages to the console
OVERRIDE_BASE_OUTPUT = 0  # Determines output based on level

defaultfilename = 'default.log'
defaultlogfile = os.path.normpath("{}/{}".format(cwd, defaultfilename))
logging.basicConfig(filename=defaultlogfile, filemode='a',
                    format='%(asctime)s  %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.DEBUG)


def updateFileName(filename):
    """
    Updates filename to a custom value.
    """
    fileh = logging.FileHandler(filename, 'a')
    formatter = logging.Formatter(
                    fmt='%(asctime)s  %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
    fileh.setFormatter(formatter)

    log = logging.getLogger()  # root logger
    for hdlr in log.handlers[:]:  # remove all old handlers
        hdlr.close()
        log.removeHandler(hdlr)
    log.addHandler(fileh)
    # Remove default filename, if exists and empty
    try:
        if os.path.getsize(defaultlogfile) == 0:
            os.remove(defaultlogfile)
    except WindowsError, e:
        logHandler('Error removing default log file: {}'.format(e),
                   type='error', output=1)


def updateLogLevel(level):
    """
    Update what gets logged to the log file; independent of the console output.
    """
    if level.lower() in ['debug', 'info', 'warning', 'error', 'critical']:
        if level.upper() == 'DEBUG':
            logging.getLogger().setLevel(logging.DEBUG)
        elif level.upper() == 'INFO':
            logging.getLogger().setLevel(logging.INFO)
        elif level.upper() == 'WARNING':
            logging.getLogger().setLevel(logging.WARNING)
        elif level.upper() == 'ERROR':
            logging.getLogger().setLevel(logging.ERROR)
        elif level.upper() == 'CRITICAL':
            logging.getLogger().setLevel(logging.CRITICAL)
    else:
        pass


def logHandler(msg, type="info", output=0):
    """
    The meat and potatoes of this module.  Determines what gets
    pushed to console as well as to the log.
    """
    base_output = checkBaseOutputLevel()
    type = type.lower()
    prefix = type.upper() + ': '
    if (type == 'critical'):
        logging.critical(msg)
        if output or 0 < base_output <= 5:
            print(Fore.YELLOW + Style.BRIGHT + Back.RED + prefix + msg)
    elif (type == 'error'):
        logging.error(msg)
        if output or 0 < base_output <= 4:
            print(Fore.RED + Style.BRIGHT + prefix + msg)
    elif (type == 'warning'):
        logging.warning(msg)
        if output or 0 < base_output <= 3:
            print(Fore.YELLOW + Style.BRIGHT + prefix + msg)
    elif (type == 'info'):
        logging.info(msg)
        if output or 0 < base_output <= 2:
            print(Fore.WHITE + Style.BRIGHT + prefix + msg)
    elif (type == 'debug'):
        try:
            GLOBAL_DEBUG
        except NameError:
            pass
        else:
            if GLOBAL_DEBUG:
                output = 1
        logging.debug(msg)
        if output or 0 < base_output == 1:
            print(Fore.WHITE + Style.DIM + prefix + msg)
    else:
        logHandler("Logging type '{}' is not a valid selection."
                   "\nMessage was: '{}'".format(type, msg),
                   type="warning", output=1)


def checkBaseOutputLevel():
    """
    OVERRIDE_BASE_OUTPUT will determine basic level to output to console,
    regardless of individual log message settings.
    This allows a convenient way to troubleshoot an issue without having to
    look at the log.

    OVERRIDE_BASE_OUTPUT value should be an integer in range 0-5.  Since this
    is user-definable, we need to check it.
    Key:
    0:  Off
    1:  Debug
    2:  Info
    3:  Warning
    4:  Error
    5:  Critical

    """
    global OVERRIDE_BASE_OUTPUT
    if OVERRIDE_BASE_OUTPUT == 0:
        return OVERRIDE_BASE_OUTPUT
    elif isinstance(OVERRIDE_BASE_OUTPUT, int):
        if 0 <= OVERRIDE_BASE_OUTPUT <= 5:
            return OVERRIDE_BASE_OUTPUT
        else:
            logHandler("OVERRIDE_BASE_OUTPUT was set to a value outside of "
                       "range 0-5; setting back to 0",
                       type="warning", output=1)
            return 0
    elif isinstance(OVERRIDE_BASE_OUTPUT, basestring):
        if OVERRIDE_BASE_OUTPUT.lower() == 'debug':
            OVERRIDE_BASE_OUTPUT = 1
        elif OVERRIDE_BASE_OUTPUT.lower() == 'info':
            OVERRIDE_BASE_OUTPUT = 2
        elif OVERRIDE_BASE_OUTPUT.lower() == 'warning':
            OVERRIDE_BASE_OUTPUT = 3
        elif OVERRIDE_BASE_OUTPUT.lower() == 'error':
            OVERRIDE_BASE_OUTPUT = 4
        elif OVERRIDE_BASE_OUTPUT.lower() == 'critical':
            OVERRIDE_BASE_OUTPUT = 5
        else:
            logHandler("OVERRIDE_BASE_OUTPUT was set to unrecognized string. "
                       "Setting to 0", type="warning", output=1)
            OVERRIDE_BASE_OUTPUT = 0
        return OVERRIDE_BASE_OUTPUT
    else:  # Unsupported string type
        logHandler("OVERRIDE_BASE_OUTPUT type can only be integer or "
                   "equivalent string value.", type="warning", output=1)
        OVERRIDE_BASE_OUTPUT = 0
        return OVERRIDE_BASE_OUTPUT
